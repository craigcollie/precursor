# [Precursor v0.2](http://github.com/craigcollie)

Oh no! It's another CSS framework. Just when you thought there couldn't possibly be another, along comes one more.


## What is Precursor

Precursor is a hard-working, lightweight CSS grid based framework, and was designed to give you a head start on your next web design project. It's simple to use and has a few basic variables that you can set up in order to make it work.

* Clone the repo: `git clone git://github.com/craigcollie/precursorcss.git`, or [Download the zip](https://github.com/craigcollie/PrecursorCSS/archive/master.zip)
* Download a copy of [SimpLESS](http://wearekiss.com/simpless) for Mac or PC
* Open up the main.less file in your favourite text editor and start creating something awesome


## Included files

* `main.less` - The file that holds it all together. Main.less loads in grid.less and mixins.less, compiles and minifies them into main.css, which gets loaded into the webpage.
* `grid.less` - This file creates the grid styles for your site. There are a few variables that you can change in this file, like padding, gutters and widths which are all calculated when the file is saved.
* `mixins.less` - This file contains a few 'most used' CSS methods like cross-browser gradients, opacity, drop-shadow etc. 


## Updates and fixes

* Precursor uses [Respond.js](https://github.com/scottjehl/Respond) for IE7 and IE8 compatibility. You can use any JS framework that add media queries for older browsers, but I've found this one to be the quickest and easiest.
* Clearfix and .clear methods are now added for convenience

## Grid.less variables

### Grid settings
* @isResponsive:true; 
    
* @blockGutter:5px;
* @outerGutter:20px;

### Responsive break points
* @desktop:960px;
* @tablet_portrait:756px;
* @tablet_landscape:959px;
* @mobile_portrait:320px;
* @mobile_landscape:480px;


## Fluid column layout
## .grid_25, .grid_33, .grid_50, .grid_66, .grid_75 and .grid_100

### Example two column layout

    <div class="grid_container">
        <div class="grid_block grid_50">
            <!-- content -->
        </div>
        <div class="grid_block grid_50">
            <!-- content -->
        </div>
    </div>

### Example three column layout
    
    <div class="grid_container">
        <div class="grid_block grid_33">
            <!-- content -->
        </div>
        <div class="grid_block grid_33">
            <!-- content -->
        </div>
        <div class="grid_block grid_33">
            <!-- content -->
        </div>
    </div>
    
### Example mixed column layout

    <div class="grid_container">
        <div class="grid_block grid_25">
            <!-- content -->
        </div>
        <div class="grid_block grid_50">
            <!-- content -->
        </div>
        <div class="grid_block grid_25">
            <!-- content -->
        </div>
    </div>
    

### Example mixed nested column layout

    <div class="grid_container">
        <div class="grid_block grid_block_nest grid_75">
            <div class="grid_row">
                <div class="grid_block grid_25">
                    <!-- content -->
                </div>
                <div class="grid_block grid_25">
                    <!-- content -->
                </div>
                <div class="grid_block grid_25">
                    <!-- content -->
                </div>
                <div class="grid_block grid_25">
                    <!-- content -->
                </div>
            </div>            
        </div>
        <div class="grid_block grid_25">
            <!-- content -->
        </div>
    </div>
   
     
## Relative column layouts 
## .grid_1 to .grid_12

### Example six column layout

    <div class="grid_container">
        <div class="grid_block grid_6">
            <!-- content -->
        </div>
        <div class="grid_block grid_6">
            <!-- content -->
        </div>
        <div class="grid_block grid_6">
            <!-- content -->
        </div>
        <div class="grid_block grid_6">
            <!-- content -->
        </div>
        <div class="grid_block grid_6">
            <!-- content -->
        </div>
        <div class="grid_block grid_6">
            <!-- content -->
        </div>
    </div>

### Example mixed nested column layout

    <div class="grid_container">
        <div class="grid_block grid_block_nest grid_2">
            <div class="grid_row">
                <div class="grid_block grid_4">
                    <!-- content -->
                </div>
                <div class="grid_block grid_4">
                    <!-- content -->
                </div>
                <div class="grid_block grid_4">
                    <!-- content -->
                </div>
                <div class="grid_block grid_4">
                    <!-- content -->
                </div>
            </div>            
        </div>
        <div class="grid_block grid_2">
            <!-- content -->
        </div>
    </div>



## I need help!

Oh, ok. Fork this project, leave a comment or drop me an email.
